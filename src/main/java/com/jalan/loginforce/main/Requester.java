package com.jalan.loginforce.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

public class Requester {

	private String protocol = "http";
	private String target = "";
	private String path = "/";
	private HttpResponse response;
	
	public String getTarget() {
		return target;
	}
	
	public Requester setTarget(String target) {
		this.target = target;
		
		return this;
	}
	
	public String getPath() {
		return path;
	}
	
	public Requester setPath(String path) {
		this.path = path;
		
		return this;
	}
	
	private String formURL() {
		return protocol + "://" + target + path;
	}
	
	public HttpResponse getResponse() {
		return this.response;
	}
	
	public Requester request(String key) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost req = new HttpPost(formURL());
		HttpResponse response;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("key", key));
		
		try {
			req.setEntity(new UrlEncodedFormEntity(params));
			
			response = httpClient.execute(req);
			
			this.response = response;
			
			return this;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean loginEval() throws UnsupportedOperationException, IOException {
		return loginEval(this.getResponse());
	}
	
	public boolean loginEval(HttpResponse response) throws UnsupportedOperationException, IOException {
		
		BufferedReader rd = new BufferedReader(
		        new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		return !result.toString().contains("invalid key");
	}
	
	
}
