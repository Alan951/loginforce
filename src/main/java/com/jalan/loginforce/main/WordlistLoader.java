package com.jalan.loginforce.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class WordlistLoader {
	
	private static WordlistLoader INSTANCE;
	private Path path;
	private int lineCursor = 0;
	private List<String> words = new ArrayList<String>();
	
	private WordlistLoader() {
		
	}
	
	public static WordlistLoader getInstance() {
		if(WordlistLoader.INSTANCE == null)
			WordlistLoader.INSTANCE = new WordlistLoader();
		
		return WordlistLoader.INSTANCE;
	}
	
	public void load(File file) throws IOException {
		this.path = Paths.get(file.getAbsolutePath());
		
		words = Files.readAllLines(this.path);
	}
	
	public void load(String filePath) throws IOException {
		File file = new File(filePath);
		
		if(!file.exists()){
			throw new FileNotFoundException();
		}
		
		load(file);
	}
	
	public List<String> getAllWords() {
		return this.words;
	}
	
	public synchronized String nextWordFromList() {
		if(this.lineCursor + 1 >= this.words.size())
			return null;
		
		String nextWord = this.words.get(lineCursor);
		
		lineCursor++;
		
		return nextWord;
	}
	
}
